package Resources;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author root
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.Serializable;
import org.json.*;

/**
 *
 * @author juan
 */
public class Message implements Serializable{
    
    private String data;
    protected static final long serialVersionUID = 1112122200L;

    
    /**
     * 
     * @param from El nombre de usuario de quien envia el mensaje
     * @param to El nombre de usuario de quien recibe el mensaje
     * @param data Información del mensaje
     */
    public Message(String data) {
        this.data = data;
    }

    public Message() {
        this.data = null;
    }

    
    /**
     * 
     * @return Regresa la información del mensaje
     */
    public String getData() {
        return data;
    }
    /**
     * 
     * @param data La información del mensaje
     */
    public void setData(String data) {
        this.data = data;
    }
    
    
    
    
}

