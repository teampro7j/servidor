/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.*;

/**
 *
 * @author juan
 */
public class ClientThread extends Thread {
    private Socket socket;
    private String name;
    private HashMap<String,ClientThread> users;
    private ObjectInputStream input;
    private ObjectOutputStream outuput;
    private DataBaseManager dataBaseManager;
    
    /***
     * Nueva instancia del thread para el cliente, se crean los objetos
     * para poder hacer la lectura y escritura de informacion
     * @param socket el socket para hacer la comunicacion servidor - cliente
     * @param dataBaseManager manejador de base de datos que nos permitira obtener informacino
     * @param name el nombre del cliente
     * @param users lista de todos los clientes con su nombre y su socket
     */
    public ClientThread(Socket socket, DataBaseManager dataBaseManager, String name, HashMap<String,ClientThread> users) {
        this.socket = socket;
        this.dataBaseManager = dataBaseManager;
        this.name = name;
        this.users = users;
        try {
            this.input = new ObjectInputStream(socket.getInputStream());
            this.outuput = new ObjectOutputStream(socket.getOutputStream());     
        } catch(Exception e) {
            System.out.println("Error en la creación del cliente " + e);
        }
    }
    
    
    /***
     * Metodo que correra por siempre, se encarga de recibir los mensajes y deacuerdo al tipo
     * (login, registro o mensaje) son el tipo de respuesta que enviara el servior
     */
    public void run() {
        boolean keepGoing = true;
        while(keepGoing) {
            //System.out.println("Esperando....");
            Resources.Message messageObject = null;
            try {
                messageObject = (Resources.Message)input.readObject();
                //System.out.println("OBJETO RECIBIDO !!");
                JSONObject dataInfo = new JSONObject(messageObject.getData());
                String type = (String) dataInfo.get("request");
                JSONObject data = dataInfo.getJSONObject("data");
                switch(type) {
                    case "registro":
                        System.out.println("PETICION DE REGISTRO");
                        try {
                            String name = (String) data.get("name");
                            String username = (String) data.get("username");
                            String password = (String) data.get("password");
                            JSONObject response = new JSONObject();
                            if(this.dataBaseManager.register(name, username, password)) {
                                System.out.println(username + " se registro");
                                response.put("response","ok");
                                response.put("data","ok");
                            } else {
                                response.put("response","no");
                                response.put("data","ok");
                            }
                            this.outuput.writeObject(new Resources.Message(response.toString()));
                            //keepGoing = false;
                        } catch(Exception e) {
                            System.out.println("Error al intentar registrar en el server " + e);
                        }
                        break;
                    case "login":
                        System.out.println("PETICION DE LOGIN");
                        try {
                            String username = (String) data.get("username");
                            String password = (String) data.get("password");
                            ArrayList<String> login = this.dataBaseManager.login(username, password);
                            JSONObject response;
                            if(login != null) {
                                //System.out.println("NO ES NULL !");
                                response = new JSONObject();
                                response.put("response","ok");
                                response.put("data",login.get(1));
                                ClientThread name = this.users.remove(this.name);
                                //change the name from guess+id to the username, in the hash map
                                this.users.put(login.get(1), name);
                                this.name = login.get(1);
                                
                            } else {
                                response = new JSONObject();
                                response.put("response","no");
                                response.put("data","a");
                            }
                            this.outuput.writeObject(new Resources.Message(response.toString()));
                        } catch(Exception e) {
                            System.out.println("Error al hacer el login en server " + e);
                        }
                        break;
                    case "mensaje":
                        System.out.println("PETICION DE MENSAJE");
                        try {
                            String to = (String) data.get("to");
                            String from = (String) data.get("from");
                            String message = (String) data.get("message");
                            //Socket toSocket = users.get(to);
                            //ObjectOutputStream outputTo = new ObjectOutputStream(toSocket.getOutputStream());
                            JSONObject response = new JSONObject();
                            JSONObject dataMessage = new JSONObject();
                            dataMessage.put("from", from);
                            dataMessage.put("to", to);
                            dataMessage.put("message", message);
                            response.put("status", "ok");
                            response.put("data", dataMessage);
                            //outputTo.writeObject(new Resources.Message(response.toString()));
                            users.get(to).getOutput().writeObject(new Resources.Message(response.toString()));
                            System.out.println("MENSAJE ENVIADO A " + to);
                        } catch(Exception e) {
                            System.out.println("Error al enviar el mensaje " + e);
                        }
                        //message logic
                        break;
                    case "users":
                        System.out.println("PETICION DE USUSARIOS");
                        try {
                            JSONObject response = new JSONObject();
                            JSONArray array = new JSONArray();
                            for(String key: users.keySet()) {
                                System.out.println("ESTA ON: " + key);
                                array.put(key);
                            }
                            response.put("status","ok");
                            response.put("data",array);
                            System.out.println("SENDING: " + response.toString());
                            this.outuput.writeObject(new Resources.Message(response.toString()));
                        } catch(Exception e) {
                            System.out.println("ERROR AL ENVIAR LOS USUARIOS " + e);
                        }
                        break;
                    case "logout":
                        System.out.println("PETICION DE DESLOGUEO");
                        try {
                            this.input.close();
                            this.outuput.close();
                            this.socket.close();
                            String name = (String) data.get("username");
                            users.remove(name);
                        } catch(Exception e) {
                            System.out.println("ERROR EN LOGOUT " + e);
                        }
                        break;
                    default:
                        break;
                }
                
            } catch (IOException ex) {
                //System.out.println("Fallo al recibir el mensaje" + ex);
            } catch (ClassNotFoundException ex) {
                System.out.println("Clase no encontrada" + ex);
            } catch (JSONException ex) {
                System.out.println("Error al crear json" + ex);
            }
            //keepGoing = false;
            
        }
    }
    
    public ObjectOutputStream getOutput() {
        return this.outuput;
    }
}
