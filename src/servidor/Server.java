/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author juan
 */
public class Server {
    private int port;
    private ServerSocket ss;
    private HashMap<String, ClientThread> clientList;
    private int id;
    
    /***
     * Nueva instancia por defecto del servidor
     */
    public Server() {
        this.id = 0;
        this.port = 555;
        this.clientList = new HashMap<String, ClientThread>();
    }
    
    /***
     * Nueva instancia con parametro del servidor
     * @param port el numero de puesrto que se usa en el socket
     */
    public Server(int port) {
        this.id = 0;
        this.port = port;
        this.clientList = new HashMap<String, ClientThread>();
    }
    
    /***
     * Inicia el servidor y espera la conexíon de clientes
     * una vez conectados los agrega a un HashMap en donde se tiene
     * el nombre del usuario y el socket que utiliza
     */
    public void start() {
        
            try {
                this.ss = new ServerSocket(this.port);
                while(true) {
                    System.out.println("Esperando a clientes para la conexion.....");
                    Socket socket = this.ss.accept();
                    System.out.println("Cliente " + this.id + " on");
                    ClientThread t = new ClientThread(socket, new DataBaseManager("messageCoco","root","root"),"guess"+this.id,clientList);
                    clientList.put("guess"+this.id,t);
                    this.id++;
                    t.start();
                }
            } catch(Exception e) {
                System.out.println("Error al crear el socket" + e);
            }
            
        
    }
       
    
}
