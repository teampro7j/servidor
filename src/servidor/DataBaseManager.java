/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author juan
 */
public class DataBaseManager {
    private Connection con;
    private String user;
    private String password;
    private String database;
    
    /**
     * Nueva instancia del manejador de base de datos
     * @param database El nombre de la base de datos
     * @param user El usuario para la conexión a la base de datos
     * @param password La contraseña para la conexión a la base de datos
     */
    public DataBaseManager(String database, String user, String password) throws SQLException {
        this.database = database;
        this.user = user;
        this.password = password;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(
                        "jdbc:mysql://localhost:3306/"+this.database+"?useSSL=false",
                        this.user,
                        this.password
                    );
        } catch(Exception e) {
            System.out.println(e);
        }
    }
    
    /***
     * Intenta insertar en la base de datos al usuario con los valores dados
     * @param name nombre a ser guardado
     * @param username usuario a ser guardado
     * @param password contraseña a ser guardada
     */
    public boolean register(String name, String username, String password) {
        String sql = "INSERT INTO users (name,username,password) VALUES (?,?,?)";
        PreparedStatement action;
        try {
            action = this.con.prepareStatement(sql);
            action.setString(1, name);
            action.setString(2, username);
            action.setString(3, password);
            action.executeUpdate();
            return true;
        } catch (SQLException ex) {
            System.out.println("Error al realizar el registro " + ex);
            return false;
        }
        //return false;
        
                
    }
    /***
     * Intenta hacer una consulta con el usuario y la contraseña, en caso de que exista
     * ese usuario regresa una lista enlazada con el nombre y usuario encontrados, en caso
     * de no existir simplemente se regresa null
     * @param username el usuario
     * @param password la contraseña del usuario
     * @return 
     */
    public ArrayList<String> login(String username, String password) {
        String sql = "SELECT name,username FROM users WHERE username = ? AND password = ?";
        ArrayList<String> res = null;
        try {
            PreparedStatement action = this.con.prepareStatement(sql);
            action.setString(1, username);
            action.setString(2, password);
            ResultSet result = action.executeQuery();
            while(result.next()){
                res = new ArrayList();
                String _name = result.getString("name");
                String _username = result.getString("username");
                
                res.add(_name);
                res.add(_username);
                return res;
            }
        } catch(SQLException ex) {
            System.out.println("Error al hacer el login en la base de datos " + ex);
        }
        return res;
    }

   
    
}
